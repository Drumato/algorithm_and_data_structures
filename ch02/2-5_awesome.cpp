#include<iostream>
#include<algorithm>
using namespace std;
static const int MAX = 200000;

int main(){
    int R[MAX], n;

    cin >> n;
    for(int i = 0;i < n;i++)cin >> R[i];

    int maxv = 0; //制約を下回る値
    int minv = R[0];

    //max()は2つの引数のうち大きい方を､
    //min()は小さい方を返す

    for(int i = 1;i < n;i++){ //毎回R[i]を読み込めば配列はいらない｡
        maxv = max(maxv,R[i] - minv); //更新
        cout << maxv;
        minv = min(minv,R[i]); //最小値の保持
    }

    cout << maxv << endl;
    return 0;
}
